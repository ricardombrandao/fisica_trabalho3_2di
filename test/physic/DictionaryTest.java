/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package physic;

import Util.Dictionary;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vitorhugo
 */
public class DictionaryTest {
    
    public DictionaryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class Dictionary.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        Dictionary expResult = Dictionary.getInstance();
        Dictionary result = Dictionary.getInstance();
        assertEquals(expResult, result);
    }

    /**
     * Test of menuName method, of class Dictionary.
     */
    @Test
    public void testMenuName() {
        System.out.println("menuName");
        Dictionary.Menu menu = Dictionary.Menu.FILE;
        Dictionary instance = Dictionary.getInstance();
        String expResult = "File";
        String result = instance.menuName(menu);
        assertEquals(expResult, result);
    }

    /**
     * Test of message method, of class Dictionary.
     */
    @Test
    public void testMessage() {
        System.out.println("message");
        Dictionary.Message message = Dictionary.Message.YES;
        Dictionary instance = Dictionary.getInstance();
        String expResult = "Yes";
        String result = instance.message(message);
        assertEquals(expResult, result);
    }

    /**
     * Test of physic method, of class Dictionary.
     */
    @Test
    public void testPhysic() {
        System.out.println("physic");
        Dictionary.Physic physic = Dictionary.Physic.ANGLE;
        Dictionary instance = Dictionary.getInstance();
        String expResult = "Angle";
        String result = instance.physic(physic);
        assertEquals(expResult, result);
    }

    /**
     * Test of currentLanguage method, of class Dictionary.
     */
    @Test
    public void testCurrentLanguage() {
        System.out.println("currentLanguage");
        Dictionary instance = Dictionary.getInstance();
        Dictionary.Language expResult = Dictionary.Language.ENGLISH;
        Dictionary.Language result = instance.currentLanguage();
        assertEquals(expResult, result);
    }
    
}
