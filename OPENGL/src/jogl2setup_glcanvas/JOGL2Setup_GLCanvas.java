package jogl2setup_glcanvas;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;
import de.matthiasmann.twl.utils.PNGDecoder;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.HashSet;
import java.util.Set;
import static javax.media.opengl.GL2.*; // GL2 constants
 
/**
 * JOGL 2.0 Program Template (GLCanvas)
 * This is a "Component" which can be added into a top-level "Container".
 * It also handles the OpenGL events to render graphics.
 */
@SuppressWarnings("serial")
public class JOGL2Setup_GLCanvas extends GLCanvas implements GLEventListener {
   // Define constants for the top-level container
   private static final String TITLE = "FISICA";  // window's title
   private static final int CANVAS_WIDTH = 640;  // width of the drawable
   private static final int CANVAS_HEIGHT = 480; // height of the drawable
   private static final int FPS = 8; // animator's target frames per second
   
   private grafic data;
   
   private static FPSAnimator animator;
 
   /** The entry main() method to setup the top-level container and animator
     * @param args */
   public static void main(String[] args) {
      // Run the GUI codes in the event-dispatching thread for thread safety
      SwingUtilities.invokeLater(new Runnable() {
         @Override
         public void run() {
            // Create the OpenGL rendering canvas
            GLCanvas canvas = new JOGL2Setup_GLCanvas();
            canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
 
            // Create a animator that drives canvas' display() at the specified FPS.
            animator = new FPSAnimator(canvas, FPS, true);
 
            // Create the top-level container
            final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
            frame.getContentPane().add(canvas);
            frame.addWindowListener(new WindowAdapter() {
               @Override
               public void windowClosing(WindowEvent e) {
                  // Use a dedicate thread to run the stop() to ensure that the
                  // animator stops before program exits.
                  new Thread() {
                     @Override
                     public void run() {
                        if (animator.isStarted()) animator.stop();
                        System.exit(0);
                     }
                  }.start();
               }
            });
            frame.setTitle(TITLE);
            frame.pack();
            frame.setVisible(true);
            animator.start(); // start the animation loop
         }
      });
   }
 
   // Setup OpenGL Graphics Renderer
 
   private GLU glu;  // for the GL Utility
 
   /** Constructor to setup the GUI for this Component */
   public JOGL2Setup_GLCanvas() {
      this.addGLEventListener(this);
   }
 
   // ------ Implement methods declared in GLEventListener ------
 
   /**
    * Called back immediately after the OpenGL context is initialized. Can be used
    * to perform one-time initialization. Run only once.
     * @param drawable
    */
   //@Override
   public void init(GLAutoDrawable drawable) {
      GL2 gl = drawable.getGL().getGL2();      // get the OpenGL graphics context
      glu = new GLU();                         // get GL Utilities
      gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // set background (clear) color
      gl.glClearDepth(1.0f);      // set clear depth value to farthest
      gl.glEnable(GL_DEPTH_TEST); // enables depth testing
      gl.glDepthFunc(GL_LEQUAL);  // the type of depth test to do
      gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // best perspective correction
      gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smoothes out lighting
      
      data = grafic.getInstance(); //carrega o objeto data onde se vai guardar a info que está na janela
      
       // ----- Your OpenGL initialization code here -----
   }
 
   /**
    * Call-back handler for window re-size event. Also called when the drawable is
    * first set to visible.
     * @param drawable
     * @param x
     * @param y
     * @param width
     * @param height
    */
   @Override
   public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
 
      if (height == 0) height = 1;   // prevent divide by zero
      float aspect = (float)width / height;
 
      // Set the view port (display area) to cover the entire window
      gl.glViewport(0, 0, width, height);
 
      // Setup perspective projection, with aspect ratio matches viewport
      gl.glMatrixMode(GL_PROJECTION);  // choose projection matrix
      gl.glLoadIdentity();             // reset projection matrix
      glu.gluPerspective(45.0, aspect, 0.1, 100.0); // fovy, aspect, zNear, zFar
 
      // Enable the model-view transform
      gl.glMatrixMode(GL_MODELVIEW);
      gl.glLoadIdentity(); // reset
   }
 
   /**
    * Called back by the animator to perform rendering.
     * @param drawable
    */
   @Override
   public void display(GLAutoDrawable drawable) {
      
    if(data.getPos_cabeca_ondaX()<8){ 
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
      gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear color and depth buffers
      gl.glLoadIdentity();  // reset the model-view matrix
 
      
      float epsilon = 0.001f;
      if(Math.abs(data.getPos_cabeca_ondaX() - data.getPos_fotaoX()) < epsilon && Math.abs(data.getPos_cabeca_ondaY() - data.getPos_fotaoY()) < epsilon){
          System.out.println("colisao");
          data.setColisao(true);
      }
      //else{
      //    data.setColisao(false);
      //    System.out.println("nao colisao");
      //}
      
      
      if(data.isColisao()){
        //data.setPos_fotaoX((float)data.getPos_fotaoX()*(float)Math.cos(data.getAng()));
        //data.setPos_fotaoY((float)data.getPos_fotaoY()*(float)Math.sin(data.getAng()));
        //data.setPos_cabeca_ondaX((float)data.getPos_cabeca_ondaX()+data.getVelocidade()*(float)Math.cos(data.getAng()));
        //data.setPos_cabeca_ondaY((float)data.getPos_cabeca_ondaY()+data.getVelocidade()*(float)Math.sin(data.getAng()));
        
        //data.setPos_cauda_ondaX((float)data.getPos_cauda_ondaX()+data.getVelocidade()*(float)Math.cos(data.getAng()));
        //data.setPos_cauda_ondaY((float)data.getPos_cauda_ondaY()+data.getVelocidade()*(float)Math.sin(data.getAng()));
          data.setAng(data.getAng_colisao());
          
      }
      
      gl.glTranslatef(0.0f, 0.0f, -12.0f); // translate into the screen
      
      gl.glPushMatrix();
        data.setPos_cabeca_ondaX(data.getPos_cabeca_ondaX()+data.getVelocidade()*(float)Math.cos(data.getAng()* Math.PI / 180.0));
        data.setPos_cabeca_ondaY(data.getPos_cabeca_ondaY()+data.getVelocidade()*(float)Math.sin(data.getAng()* Math.PI / 180.0));
        //data.setPos_cauda_ondaX(data.getPos_cauda_ondaX()+data.getVelocidade()*(float)Math.cos(data.getAng()* Math.PI / 180.0));
        //data.setPos_cauda_ondaY(data.getPos_cauda_ondaY()+data.getVelocidade()*(float)Math.sin(data.getAng()* Math.PI / 180.0));
        gl.glColor3f(1.0f, 0.0f, 0.0f);
        gl.glTranslatef(data.getPos_cabeca_ondaX(), data.getPos_cabeca_ondaY(), 0.0f);
        gl.glRotatef(data.getAng(), 0.0f, 0.0f, 1.0f);
        gl.glBegin(GL_LINES);
            gl.glVertex2f(-0.5f, 0.0f);
            gl.glVertex2f(0.0f, 0.0f);
        gl.glEnd();
      gl.glPopMatrix();
      
      
      
      gl.glPushMatrix();      
      gl.glColor3f(1.0f, 0.0f, 0.0f);
      gl.glBegin(GL_LINE_LOOP);
	for (int i = 0; i < 360; ++i){
            gl.glVertex2f(data.getPos_fotaoX() + ((float)Math.cos((i*3.14) / 180)*data.getRaio_fotao()), data.getPos_fotaoY() + ((float)Math.sin((i*3.14) / 180)*data.getRaio_fotao()));
	}
      gl.glEnd();
      gl.glPopMatrix();
      
      
      
      
      
      
   }else{
          animator.stop();
      }
      
   }
   
 
   /**
    * Called back before the OpenGL context is destroyed. Release resource such as buffers.
     * @param drawable
    */
   @Override
   public void dispose(GLAutoDrawable drawable) { }

}