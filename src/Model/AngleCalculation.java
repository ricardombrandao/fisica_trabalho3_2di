/**
 *
 */
package Model;

/**This is a subclass of the class calculation and its function is to  hold the
 *the resultant angle from a initial wavelength and a final wavelength.
 *
 * @author VitorHugo
 *
 */
public class AngleCalculation extends Calculation {

    private final double _initial_wavelength;
    private final double _final_wavelength;
    private final double _angle;
	
	/**
	*Constructor with an argument for name, result, initial wavelength and final wavelength.
	*@param name the calculation name
	*@param initial_wavelength the initial wavelength
	*@param final_wavelength the final wavelength
	*@param result the result from the calculation
	*
	*@see Calculation Super-class of this class
	*/
    public AngleCalculation(String name, double initial_wavelength,
            double final_wavelength, double result) {
        super(name, result);
        _initial_wavelength = initial_wavelength;
        _final_wavelength = final_wavelength;
        _angle = result;
    }
    
	/**
	*Returns the initial wavelength.
	*@return the initial wavelength.
	*/
    public double getInitialWavelength(){
        return _initial_wavelength;
    }
    
	/**
	*Returns the final wavelength.
	*@return the final wavelength.
	*/
    public double getFinalWavelength(){
        return _final_wavelength;
    }
    
	/**
	*Returns the angle.
	*@return the angle.
	*/
    public double getAngle(){
        return _angle;
    }
    
	
	/**
	*Returns the angle as a formatted String.
	*@return the angle as a formatted String.
	*/
    public String angleToString(){
        return String.format("%.0f\u00B0", _angle);
    }
    
	
	/**
	*Returns the initial wavelength as a formatted String.
	*@return the initial wavelength as a formatted String.
	*/
    public String initialWavelengthToString(){
        return String.format("%.3fnm", _initial_wavelength);
    }
    
	
	/**
	*Returns the final wavelength as a formatted String.
	*@return the final wavelength as a formatted String.
	*/
    public String finalWavelengthToString(){
        return String.format("%.3fnm", _final_wavelength);
    }

	
    @Override
    public String toolTipText() {
        return String.format(
                "\u03BB = %.3fpm, \u03BB\u0384 = %fpm, \u03B8 = %.0f\u00B0",
                _initial_wavelength, _final_wavelength, _angle);
    }

    @Override
    public String toString() {
        return super.get_calculation_name();
    }
}
