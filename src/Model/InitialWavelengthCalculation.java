/**
 *
 */
package Model;

/**
 * @author VitorHugo
 *
 */
public class InitialWavelengthCalculation extends Calculation {

    private final double _final_wavelength;
    private final double _angle;

    public InitialWavelengthCalculation(String name, double final_wavelength,
            double angle, double result) {
        super(name, result);
        _final_wavelength = final_wavelength;
        _angle = angle;
    }

    /**
     * @return the _final_wavelength
     */
    public double get_final_wavelength() {
        return _final_wavelength;
    }

    /**
     * @return the _angle
     */
    public double get_angle() {
        return _angle;
    }
	
	/**
	*Returns the final wavelength as a formatted String.
	*@return the final wavelength as a formatted String.
	*/    
    public String finalWavelengthToString(){
        return String.format("%.3fnm", _final_wavelength);
    }
    	
	/**
	*Returns the angle as a formatted String.
	*@return the angle as a formatted String.
	*/
    public String angleToString(){
        return String.format("%.0f\u00B0", _angle);
    }
    	
	/**
	*Returns the initial wavelength as a formatted String.
	*@return the initial wavelength as a formatted String.
	*/
    public String initialWavelengthToString(){
        return String.format("%snm", super.toString());
    }

    @Override
    public String toolTipText() {
        return String.format(
                "\u03BB\u0384 = %.3fpm, \u03B8 = %.0f\u00B0, \u03BB = %sm",
                _final_wavelength,
                _angle, super.toString());
    }

    @Override
    public String toString() {
        return get_calculation_name();
    }
}
