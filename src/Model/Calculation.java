/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DecimalFormat;

/**
 *Abstraction of a calculation, holds the necessary values for a calculation and its result, this is a superclass.
 Implements Serializable so we can save data to binary file.
 
 * @author vitorhugo
 */
public abstract class Calculation implements Serializable {
    
	private final String _calculation_name;
	private final double _calculation_result;

    private final Timestamp timestamp;

	/**
	*Constructor with an argument for name and result.
	*@param name the calculation name
	*@param result the result from the calculation
	*/
	public Calculation(String name,
			double result) {
		_calculation_name = name;
		_calculation_result = result;
        java.util.Date date= new java.util.Date();
        this.timestamp =  new Timestamp(date.getTime());
    }


    /**
	 * Returns the calculation name.
	 * @return the _calculation_name.
	 */
	public String get_calculation_name() {
		return _calculation_name;
	}

	/**
	 * Returns the calculation result.
	 * @return the _calculation_result
	 */
	public double get_calculation_result() {
		return _calculation_result;
	}


	/**
	 * Returns a timestamp of the date of creation.
	 * @return the timestamp
	 */
    public Timestamp getTimestamp() {
        return timestamp;
    }

	/**
	* Returns a formatted String to be displayed as a tool tip.
	* @return returns a formatted String to be displayed as a tool tip
	*/
	public abstract String toolTipText();

    @Override
    public String toString() {
		DecimalFormat df = new DecimalFormat("0.000E0");
		return df.format(_calculation_result);
    }
    
    
    
    
}
