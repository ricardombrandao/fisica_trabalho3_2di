/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *This class is responsible for the calculation of angle, initial wavelength and final wavelength.
 * @author Tiago Faria
 */
public class Calculator {
    
    private static double inicialWaveLength;
    private static double finalWaveLength;
    private static double angle;
    private static double mElec = 9.1093897*Math.pow(10,-31); //Massa do Electrão
    private static double h = 6.6260693*Math.pow(10,-34); //Constante de Planck
    private static double c = 299792458; //Velocidade da Luz (m/s)
    
	/**
	* Empty constructor
	*/
    public Calculator() {
        
    }
    
	/**
	* Edits the initial wavelength
	*@param iWaveLen the new initial wavelength
	*/
    public static void setInicialWaveLength(double iWaveLen) {
        inicialWaveLength = (iWaveLen > 0) ? iWaveLen*Math.pow(10,-12) : 1;
    }
    
	/**
	* Edits the final wavelength
	*@param fWaveLen the new final wavelength
	*/ 
    public void setFinalWaveLength(double fWaveLen) {
        finalWaveLength = (fWaveLen > 0) ? fWaveLen*Math.pow(10,-11) : 1;
    }
     
	/**
	* Edits the angle
	*@param ang the new angle
	*/
    public static void setAngle(double ang) {
        angle = (30 < ang && ang < 160) ? ang : 1;
    }
    
	/**
	* Calculates the initial wavelength
	*@param fWaveLen the final wavelength
	*@param ang the angle
	*/
    public double calculateInicialWaveLength(double fWaveLen, double ang) {
        setFinalWaveLength(fWaveLen);
        setAngle(ang);
        double consta = h/(mElec*c);
        inicialWaveLength = finalWaveLength - consta*(1-Math.cos(angle));
        return inicialWaveLength;
    }
    
	/**
	* Calculates the final wavelength
	*@param iWaveLen the initial wavelength
	*@param ang the angle
	*/
    public static double calculateFinalWaveLength(double iWaveLen, double ang) {
        setInicialWaveLength(iWaveLen);
        setAngle(ang); 
        double consta = h/(mElec*c);
        finalWaveLength = consta*(1-Math.cos(angle))+inicialWaveLength;
        return finalWaveLength;
    }
    
	/**
	* Calculates the angle
	*@param iWaveLen the initial wavelength
	*@param fWaveLen the final wavelength
	*/
    public double calculateAngle(double iWaveLen, double fWaveLen) {
        setInicialWaveLength(iWaveLen);
        setFinalWaveLength(fWaveLen);
        double cos;
        double consta = h/(mElec*c);
        cos = 1-((finalWaveLength - inicialWaveLength)/consta);
        angle = Math.cosh(cos);
        return angle;
    }
    
}
