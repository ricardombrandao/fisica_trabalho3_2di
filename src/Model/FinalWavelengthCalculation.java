/**
 *
 */
package Model;

/**This is a subclass of the class calculation and its function is to  hold the
 *the resultant final wavelength from a initial wavelength and a angle.
 *
 * @author VitorHugo
 *
 */
public class FinalWavelengthCalculation extends Calculation {

    private final double _initial_wavelength;
    private final double _angle;

	/**
	*Constructor with an argument for name, result, initial wavelength and angle.
	*@param name the calculation name
	*@param initial_wavelength the initial wavelength
	*@param angle the angle
	*@param result the result from the calculation
	*
	*@see Calculation Super-class of this class
	*/
    public FinalWavelengthCalculation(String name, double initial_wavelength,
            double angle, double result) {
        super(name, result);
        _initial_wavelength = initial_wavelength;
        _angle = angle;
    }
    	
	/**
	*Returns the initial wavelength as a formatted String.
	*@return the initial wavelength as a formatted String.
	*/
    public String initialWavelengthToString(){
        return String.format("%.3fnm", _initial_wavelength);
    }
    	
	/**
	*Returns the final wavelength as a formatted String.
	*@return the final wavelength as a formatted String.
	*/
    public String finalWavelengthToString(){
        return String.format("%snm", super.toString());
    }
    	
	/**
	*Returns the angle as a formatted String.
	*@return the angle as a formatted String.
	*/
    public String angleToString(){
        return String.format("%.0f\u00B0", _angle);
    }

    @Override
    public String toolTipText() {
        return String.format(
                "\u03BB = %.3fpm, \u03B8 = %.0f\u00B0, \u03BB\u0384 = %sm",
                _initial_wavelength,
                _angle, super.toString());
    }

    @Override
    public String toString() {
        return get_calculation_name();
    }
}
