/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JPanel;

import Model.AngleCalculation;
import Model.Calculator;
import Model.FinalWavelengthCalculation;
import Model.InitialWavelengthCalculation;
import Persistence.Repository;
import Util.HTMLExport;
import View.InputUI;
import View.MainUI;

/**
 * This class is a controller responsible for the interaction of the calculator
 * model with the view It uses a actionlistener to detect when the user clicks
 * the button from the view and then asks the model to do the calculation,
 * afterwards it updates the view fields with the result.
 *
 * @author vitorhugo
 */
public class WavelengthController {

    private final Calculator the_model;
    private final JPanel the_view;
    private final MainUI the_history_view;

    /**
     * Constructor
     *
     * @param the_model the model, in this case, the class responsible for the
     * calculation
     * @param the_view the view with input fields for wavelength and angle
     */
    public WavelengthController(Calculator the_model, JPanel the_view,
            MainUI frame) {
        this.the_model = the_model;
        this.the_view = the_view;
        this.the_history_view = frame;

        // this will let us update the view when the user clicks the button
        ((InputUI) this.the_view)
                .addCalculateFinalWavelengthListener(new CalculateFinalWavelengthListener());
        ((InputUI) this.the_view)
                .addCalculateInitialWavelengthListener(new CalculateInitialWavelengthListener());
        ((InputUI) this.the_view)
                .addCalculateAngleListener(new CalculateAngleListener());
    }

    private static class ExportListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            HTMLExport a = new HTMLExport();
            a.writeFile();
        }
    }

    /**
     * ActionListener reponsible for the actions on the view, receives data from
     * the view and updates it with the resulting data.
     */
    class CalculateAngleListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String name = "";
            double initial_wavelength = 0, final_wavelength = 0, result = 0;
            DecimalFormat df = new DecimalFormat("0.000E0");

            try {

                name = ((InputUI) the_view).getNameCA();
                initial_wavelength = ((InputUI) the_view)
                        .getInitialWavelengthCA();
                final_wavelength = ((InputUI) the_view).getFinalWavelengthCA();

                result = the_model.calculateAngle(initial_wavelength,
                        final_wavelength);

                ((InputUI) the_view).setResultCA(df.format(result));

                Repository.getInstance().save(
                        new AngleCalculation(name, initial_wavelength,
                        final_wavelength, result));

                the_history_view.updateList();

            } catch (NumberFormatException ex) {

                ((InputUI) the_view)
                        .displayErrorMessage("You need to enter a valid initial wavelength and final wavelength");

            }
        }
    }

    /**
     * ActionListener reponsible for the actions on the view, receives data from
     * the view and updates it with the resulting data.
     */
    class CalculateFinalWavelengthListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String name = "";
            double initia_wavelength = 0, angle = 0, result = 0;
            DecimalFormat df = new DecimalFormat("0.000E0");

            try {

                name = ((InputUI) the_view).getNameCFW();
                initia_wavelength = ((InputUI) the_view).getInitialWavelengthCFW();
                angle = ((InputUI) the_view).getAngleCFW();
                result = the_model.calculateFinalWaveLength(initia_wavelength, angle);


                ((InputUI) the_view).setResultCFW(df.format(result));

                Repository.getInstance().save(
                        new FinalWavelengthCalculation(name, initia_wavelength,
                        angle,
                        result));

                the_history_view.updateList();

            } catch (NumberFormatException ex) {


                ((InputUI) the_view)
                        .displayErrorMessage("You need to enter a valid initial wavelength and angle");

            }
        }
    }

    /**
     * ActionListener reponsible for the actions on the view, receives data from
     * the view and updates it with the resulting data.
     */
    class CalculateInitialWavelengthListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String name = "";
            double final_wavelength = 0, angle = 0, result = 0;
            DecimalFormat df = new DecimalFormat("0.000E0");

            try {

                name = ((InputUI) the_view).getNameCIW();
                final_wavelength = ((InputUI) the_view).getFinalWavelengthCIW();
                angle = ((InputUI) the_view).getAngleCIW();

                result = the_model
                        .calculateInicialWaveLength(final_wavelength, angle);

                ((InputUI) the_view).setResultCIW(df.format(result));

                Repository.getInstance().save(
                        new InitialWavelengthCalculation(name, final_wavelength,
                        angle,
                        result));

                the_history_view.updateList();

            } catch (NumberFormatException ex) {

                ((InputUI) the_view)
                        .displayErrorMessage("You need to enter a valid final wavelength and angle");

            }
        }
    }
}
