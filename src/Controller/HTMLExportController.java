/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Util.HTMLExport;
import View.MainUI;

/**
 *
 * @author vitorhugo
 */
public class HTMLExportController {
    
    private final HTMLExport the_model;
	private final MainUI the_view;

	public HTMLExportController(HTMLExport the_model, MainUI the_view) {
        this.the_model = the_model;
        this.the_view = the_view;
    }
    
    public static class ExportListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            HTMLExport a = new HTMLExport();
            a.writeFile();
        }
    }
    
    
}
