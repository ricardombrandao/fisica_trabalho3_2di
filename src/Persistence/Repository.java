/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import Model.AngleCalculation;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import Model.Calculation;
import Model.FinalWavelengthCalculation;
import Model.InitialWavelengthCalculation;

/**
 * This class is responsible for the persistence of calculations.
 * @author vitorhugo
 */
public class Repository {

    private static Repository instance;
    private ArrayList<Calculation> list;

    private Repository() {
        list = new ArrayList<Calculation>();
    }

    /**
     * Repository is singleton, this method returns an instance of Repository
     *
     * @return returns an instance of Repository
     */
    public static Repository getInstance() {
        if (instance == null) {
            instance = new Repository();
        }

        return instance;
    }

    /**
     * Saves a calculation into the repository
     *
     * @param calculation the calculation to be saved
     */
    public void save(Calculation calculation) {
        list.add(calculation);
    }

    /**
     * Returns the list of calculations saved
     *
     * @return the list of calculations saved
     */
    public ArrayList<Calculation> getList() {
        return list;
    }

    /**
     * Loads data from the binary file
     */
    public void loadData() {

        try {
            File file = new File("data.bin");
            if (file.exists()) {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream("data.bin"));
                list = (ArrayList<Calculation>) in.readObject();
                in.close();
            }

        } catch (IOException exc) {
            exc.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Saves data into a binary file
     */
    public void saveData() {

        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data.bin"));
            out.writeObject(list);
            out.close();
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
	/**
     * Returns the last five results from the list
     *
     * @return the last five results from the list
     */
    public Object[] last5Results() {
        return list.size() >= 5 ? list.subList(list.size() - 5, list.size())
                .toArray() : list.toArray();
    }

	/**
     * Returns true if the list has initial wavelength calculations
     *
     * @return true if the list has initial wavelength calculations
     */
    public boolean hasInitialWavelengthCalculations() {

        for (Calculation calculation : list) {

            if (calculation instanceof InitialWavelengthCalculation) {
                return true;
            }
        }
        return false;
    }

	/**
     * Returns true if the list has final wavelength calculations
     *
     * @return true if the list has final wavelength calculations
     */
    public boolean hasFinalWavelengthCalculations() {

        for (Calculation calculation : list) {

            if (calculation instanceof FinalWavelengthCalculation) {
                return true;
            }
        }
        return false;
    }

	/**
     * Returns true if the list has angle calculations
     *
     * @return true if the list has angle wavelength calculations
     */
    public boolean hasAngleCalculations() {

        for (Calculation calculation : list) {

            if (calculation instanceof AngleCalculation) {
                return true;
            }
        }
        return false;
    }
}
