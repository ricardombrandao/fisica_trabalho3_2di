/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package View;

/**
 *
 * @author ScarePT
 */
public class grafic {
    
    private float onda_entrada;
    private float onda_saida;
    private float ang;
    private float pos_fotaoX;
    private float pos_fotaoY;
    private float pos_cabeca_ondaX;
    private float pos_cabeca_ondaY;
    private float raio_fotao;
    private boolean colisao;
    private float velocidade;
    private float pos_cauda_ondaX;
    private float pos_cauda_ondaY;
    //ANGULO DE INCIDENCIA É SEMPRE O MESMO = 0.0f
    
    // Variavel estática que conterá a instancia do método
     private static grafic instance;
 
     // Construtor privado. Suprime o construtor público padrao.
     private grafic() {
          // Operações de inicialização da classe
          onda_entrada=0.0f;
          onda_saida=0.0f;
          ang=90.0f;
          pos_fotaoX=2.0f;
          pos_fotaoY=0.0f;
          pos_cabeca_ondaX=0.0f;
          pos_cabeca_ondaY=0.0f;
          raio_fotao = 0.2f;
          colisao = false;
          velocidade = 0.1f;
          pos_cabeca_ondaX=+0.5f;
          pos_cabeca_ondaY=0.0f;
     }
 
     // Método público estático de acesso único ao objeto!
     public static grafic getInstance(){
           // Verifica se a variável possui algum valor,caso não, é criada a instancia.
           if (instance == null) {
               instance = new grafic();
           }
           // Se a variavel possui algum valor, é retornado para quem está pedindo
           return instance;
     }

    /**
     * @return the onda_entrada
     */
    public float getOnda_entrada() {
        return onda_entrada;
    }

    /**
     * @param onda_entrada the onda_entrada to set
     */
    public void setOnda_entrada(float onda_entrada) {
        this.onda_entrada = onda_entrada;
    }

    /**
     * @return the onda_saida
     */
    public float getOnda_saida() {
        return onda_saida;
    }

    /**
     * @param onda_saida the onda_saida to set
     */
    public void setOnda_saida(float onda_saida) {
        this.onda_saida = onda_saida;
    }

    /**
     * @return the ang
     */
    public float getAng() {
        return ang;
    }

    /**
     * @param ang the ang to set
     */
    public void setAng(float ang) {
        this.ang = ang;
    }

    /**
     * @return the pos_fotao
     */
    public float getPos_fotaoX() {
        return pos_fotaoX;
    }

    /**
     * @param pos_fotaoX the pos_fotao to set
     */
    public void setPos_fotaoX(float pos_fotaoX) {
        this.pos_fotaoX = pos_fotaoX;
    }
    
    /**
     * @return the pos_fotao
     */
    public float getPos_fotaoY() {
        return pos_fotaoY;
    }

    /**
     * @param pos_fotaoY the pos_fotao to set
     */
    public void setPos_fotaoY(float pos_fotaoY) {
        this.pos_fotaoY = pos_fotaoY;
    }

    /**
     * @return the pos_cabeca_ondaX
     */
    public float getPos_cabeca_ondaX() {
        return pos_cabeca_ondaX;
    }

    /**
     * @param pos_cabeca_ondaX the pos_cabeca_onda to set
     */
    public void setPos_cabeca_ondaX(float pos_cabeca_ondaX) {
        this.pos_cabeca_ondaX = pos_cabeca_ondaX;
    }/**
     * @return the pos_cabeca_ondaY
     */
    public float getPos_cabeca_ondaY() {
        return pos_cabeca_ondaY;
    }

    /**
     * @param pos_cabeca_ondaY the pos_cabeca_ondaY to set
     */
    public void setPos_cabeca_ondaY(float pos_cabeca_ondaY) {
        this.pos_cabeca_ondaY = pos_cabeca_ondaY;
    }

    /**
     * @return the raio_fotao
     */
    public float getRaio_fotao() {
        return raio_fotao;
    }

    /**
     * @return the colisao
     */
    public boolean isColisao() {
        return colisao;
    }

    /**
     * @param colisao the colisao to set
     */
    public void setColisao(boolean colisao) {
        this.colisao = colisao;
    }

    /**
     * @return the velocidade
     */
    public float getVelocidade() {
        return velocidade;
    }

    /**
     * @return the pos_cauda_ondaX
     */
    public float getPos_cauda_ondaX() {
        return pos_cauda_ondaX;
    }

    /**
     * @param pos_cauda_ondaX the pos_cauda_ondaX to set
     */
    public void setPos_cauda_ondaX(float pos_cauda_ondaX) {
        this.pos_cauda_ondaX = pos_cauda_ondaX;
    }

    /**
     * @return the pos_cauda_ondaY
     */
    public float getPos_cauda_ondaY() {
        return pos_cauda_ondaY;
    }

    /**
     * @param pos_cauda_ondaY the pos_cauda_ondaY to set
     */
    public void setPos_cauda_ondaY(float pos_cauda_ondaY) {
        this.pos_cauda_ondaY = pos_cauda_ondaY;
    }
}
