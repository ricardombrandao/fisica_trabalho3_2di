package View;

import java.awt.Graphics;
import java.awt.Toolkit;

import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JComponent;

/**
 * 
 * A custom Button that extends JButton and loads an image to the button
 * 
 */
@SuppressWarnings("serial")
public class CustoButton extends JButton {

	/**
	 * the image to the button
	 */
	private String image;

	/**
	 * the image to the button when the mouse is over the button
	 */
	private String imageRollOver;

	/**
	 * Whether or not we've set the RollOver image.
	 */
	private boolean hasRollOver = false;

	/**
	 * Constructor of this class, loads an image
	 * 
	 * @param botao
	 *            the path of the image
	 */
	public CustoButton(String button) {

		this.image = button;


	}

	/**
	 * Constructor of this class, loads an image to the button and other to the
	 * roll over option
	 * 
	 * @param botao
	 *            the path of the image
	 * @param btnRoll
	 *            the path of the roll over image
	 */
	public CustoButton(String button, String btnRoll) {

		this.image = button;
		this.imageRollOver = btnRoll;
		hasRollOver = true;

	}

	public void update(String newButton, String newRoll) {
		this.image = newButton;
		this.imageRollOver = newRoll;
		hasRollOver = true;
	}

	/**
	 * Draws the image into the button
	 * 
	 * @see JComponent
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(
				Toolkit.getDefaultToolkit().getImage(
						getClass().getResource("/images/" + image)), 0, 0,
				null);
		repaint();
		ButtonModel model = getModel();
		if (hasRollOver) {
			if (model.isRollover()) {
				g.drawImage(
						Toolkit.getDefaultToolkit().getImage(
								getClass().getResource(
										"/images/" + imageRollOver)), 0, 0,
						null);
				repaint();
			}
		}
	}



}
