/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 
package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import Util.Dictionary;
import java.awt.CardLayout;
import java.awt.Dimension;

 *//**
 *
 * @author Scare
 */
/*
 * public class InputPanel extends JPanel {
 * 
 * private JTextField wavelength_field = new JTextField(10); private JLabel
 * wavelength_label = new
 * JLabel(Dictionary.getInstance().physic(Dictionary.Physic.WAVELENGTH));
 * private JLabel angle_label = new
 * JLabel(Dictionary.getInstance().physic(Dictionary.Physic.ANGLE)); private
 * JLabel result_label = new
 * JLabel(Dictionary.getInstance().physic(Dictionary.Physic.FINAL_WAVELENGTH));
 * private JTextField angle_field = new JTextField(10); private JButton
 * calculateButton = new
 * JButton(Dictionary.getInstance().physic(Dictionary.Physic.CALCULATE));
 * private JTextField result = new JTextField(10); JPanel first_option_panel =
 * new JPanel(new BorderLayout()); JPanel second_option_panel = new JPanel(new
 * BorderLayout()); JPanel third_option_panel = new JPanel(); CardLayout cl =
 * new CardLayout(10, 10); JPanel main_panel = new JPanel(cl);
 * 
 * public InputPanel() { super(); this.setBackground(Color.red);
 * buildFirtOptionPanel(); buildSecondOptionPanel(); add(main_panel); }
 * 
 * 
 * private void buildFirtOptionPanel(){ first_option_panel.add(new
 * JLabel(Dictionary.getInstance().physic(Dictionary.Physic.ANGLE)));
 * 
 * JPanel fields_panel = new JPanel(new GridLayout(3, 2)); JPanel calcPanel =
 * new JPanel();
 * 
 * fields_panel.add(new
 * JLabel(Dictionary.getInstance().physic(Dictionary.Physic.ANGLE)));
 * fields_panel.add(new JTextField()); fields_panel.add(new
 * JLabel(Dictionary.getInstance().physic(Dictionary.Physic.ANGLE)));
 * fields_panel.add(new JTextField()); fields_panel.add(new
 * JLabel(Dictionary.getInstance().physic(Dictionary.Physic.ANGLE)));
 * fields_panel.add(new JTextField()); calcPanel.add(new JButton("AS"));
 * 
 * first_option_panel.add(fields_panel, BorderLayout.NORTH);
 * first_option_panel.add(calcPanel, BorderLayout.SOUTH);
 * main_panel.add(first_option_panel, "0");
 * 
 * }
 * 
 * private void buildSecondOptionPanel(){
 * second_option_panel.add(wavelength_label);
 * 
 * JPanel fields_panel2 = new JPanel(new GridLayout(3, 2)); JPanel calcPanel2 =
 * new JPanel();
 * 
 * fields_panel2.add(new JLabel("IAJSDA")); fields_panel2.add(wavelength_field);
 * fields_panel2.add(angle_label); fields_panel2.add(angle_field);
 * fields_panel2.add(result_label); fields_panel2.add(result);
 * calcPanel2.add(calculateButton);
 * 
 * result.setEditable(false);
 * 
 * second_option_panel.add(fields_panel2, BorderLayout.NORTH);
 * second_option_panel.add(calcPanel2, BorderLayout.SOUTH);
 * main_panel.add(second_option_panel, "2"); }
 * 
 * public void getCardOption(int option) { cl.show(main_panel, "" + option); }
 * 
 * 
 * public double getWavelength() {
 * 
 * return Double.parseDouble(wavelength_field.getText());
 * 
 * }
 * 
 * public double getAngle() {
 * 
 * return Double.parseDouble(angle_field.getText());
 * 
 * }
 * 
 * public double getResult() {
 * 
 * return Double.parseDouble(result.getText());
 * 
 * }
 * 
 * public void setResult(String solution) {
 * 
 * result.setText(solution);
 * 
 * }
 * 
 * // If the calculateButton is clicked execute a method // in the Controller
 * named actionPerformed public void addCalculateListener(ActionListener
 * listenForCalcButton) {
 * 
 * calculateButton.addActionListener(listenForCalcButton); }
 * 
 * // Open a popup that contains the error message passed public void
 * displayErrorMessage(String errorMessage) {
 * 
 * JOptionPane.showMessageDialog(this, errorMessage);
 * 
 * }
 * 
 * public void updateFields() {
 * wavelength_label.setText((Dictionary.getInstance(
 * ).physic(Dictionary.Physic.WAVELENGTH)));
 * angle_label.setText((Dictionary.getInstance
 * ().physic(Dictionary.Physic.ANGLE)));
 * result_label.setText((Dictionary.getInstance
 * ().physic(Dictionary.Physic.FINAL_WAVELENGTH)));
 * calculateButton.setText((Dictionary
 * .getInstance().physic(Dictionary.Physic.CALCULATE))); } }
 */