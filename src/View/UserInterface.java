/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 
package View;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;

import Controller.HTMLExportController;
import Model.Calculator;
import Persistence.Repository;
import Util.Dictionary;
import Util.HTMLExport;

 *//**
 *
 * @author Scare
 */
/*
 *//**
 * @author VitorHugo
 *
 */
/*
 * public class UserInterface extends JFrame {
 * 
 * private final JMenu menu1; private final JMenu menu2; private final JMenu
 * menu3; private final JMenu menu4; private final JMenu menu5; //Menu FILE
 * private final JMenuItem menu1Item1; private final JMenuItem menu1Item2;
 * private final JMenuItem menu1Item3; //MENU VIEW private final JMenuItem
 * menu2Item1; private final JMenuItem menu2Item2; //MENU OTHERS private final
 * JMenuItem menu3Item1; //MENU HELP private final JMenuItem menu4Item1; //MENU
 * LANGUAGE private final JMenuItem menu5Item1; private final JMenuItem
 * menu5Item2; private final InputPanel painelInput;
 * 
 * public UserInterface() { super();
 * 
 * //============================================ //Definir DimensÃ£o da Janela
 * //============================================ Dimension dim_ecra =
 * Toolkit.getDefaultToolkit().getScreenSize(); if (dim_ecra.getWidth() > 1280
 * && dim_ecra.getHeight() > 800) { setSize(1000, 600); } else { double
 * height_ecra = dim_ecra.height * 0.8; double width_ecra = dim_ecra.width *
 * 0.8; setSize((int) width_ecra, (int) height_ecra); }
 * 
 * //============================================ //Define a localizaÃ§Ã£o da
 * abertura da janela //============================================
 * setLocation((dim_ecra.width) / 2 - (this.getWidth() / 2), (dim_ecra.height) /
 * 2 - (this.getHeight() / 2));
 * 
 * //============================================ //Para aparecer a
 * confirmaÃ§Ã£o de saida do programa
 * //============================================
 * setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); addWindowListener(new
 * WindowAdapter() {
 * 
 * @Override public void windowClosing(WindowEvent e) { Object[] options =
 * {Dictionary.getInstance().message(Dictionary.Message.YES),
 * Dictionary.getInstance().message(Dictionary.Message.NO)}; int i =
 * JOptionPane.showOptionDialog(null,
 * Dictionary.getInstance().message(Dictionary.Message.CONFIRM_EXIT),
 * Dictionary.getInstance().message(Dictionary.Message.EXIT),
 * JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
 * options[0]); if (i == JOptionPane.YES_OPTION) { System.exit(0); } else {
 * repaint(); } } });
 * 
 * //============================================ //Definir tamanho minimo
 * janela //============================================ Dimension dim_janela =
 * this.getSize(); double height_janela = dim_janela.height * 0.8; double
 * width_janela = dim_janela.width * 0.8; Dimension size_min = new
 * Dimension((int) width_janela, (int) height_janela);
 * this.setMinimumSize(size_min);
 * 
 * //=========================================== // Controller para htmlexport
 * //=========================================== HTMLExportController
 * htmlController = new HTMLExportController(new HTMLExport(), this);
 * 
 * //============================================ //DefiniÃ§Ã£o dos menus
 * //============================================ JMenuBar menuBar = new
 * JMenuBar();
 * 
 * Dictionary.getInstance().changeLanguage(Dictionary.Language.ENGLISH);
 * 
 * menu1 = new JMenu(Dictionary.getInstance().menuName(Dictionary.Menu.FILE));
 * menu2 = new JMenu(Dictionary.getInstance().menuName(Dictionary.Menu.VIEW));
 * menu3 = new JMenu(Dictionary.getInstance().menuName(Dictionary.Menu.OTHERS));
 * menu4 = new JMenu(Dictionary.getInstance().menuName(Dictionary.Menu.HELP));
 * menu5 = new
 * JMenu(Dictionary.getInstance().menuName(Dictionary.Menu.LANGUAGE));
 * 
 * //Menu FILE menu1Item1 = new
 * JMenuItem(Dictionary.getInstance().menuName(Dictionary.Menu.SAVE));
 * menu1Item2 = new
 * JMenuItem(Dictionary.getInstance().menuName(Dictionary.Menu.EXPORT_HTML));
 * menu1Item3 = new
 * JMenuItem(Dictionary.getInstance().menuName(Dictionary.Menu.EXIT)); //MENU
 * VIEW menu2Item1 = new
 * JMenuItem(Dictionary.getInstance().menuName(Dictionary.Menu.GRAPH));
 * menu2Item2 = new
 * JMenuItem(Dictionary.getInstance().menuName(Dictionary.Menu.CALCULATE_ANGLE
 * )); //MENU OTHERS menu3Item1 = new
 * JMenuItem(Dictionary.getInstance().menuName(Dictionary.Menu.CURIOSITIES));
 * //MENU HELP menu4Item1 = new
 * JMenuItem(Dictionary.getInstance().menuName(Dictionary.Menu.WIKI)); //MENU
 * LANGUAGE ButtonGroup grupoBotoes = new ButtonGroup(); JRadioButtonMenuItem
 * lang1 = new
 * JRadioButtonMenuItem(Dictionary.getInstance().menuName(Dictionary.
 * Menu.PORTUGUESE),new ImageIcon("images/PT.gif")); JRadioButtonMenuItem lang2
 * = new
 * JRadioButtonMenuItem(Dictionary.getInstance().menuName(Dictionary.Menu.ENGLISH
 * ),new ImageIcon("images/ENG.gif")); grupoBotoes.add(lang1);
 * grupoBotoes.add(lang2); menu5Item1 = lang1; menu5Item2 = lang2;
 * loadSelectedLangButton();
 * 
 * 
 * menuBar.add(menu1); menuBar.add(menu2); menuBar.add(menu3);
 * menuBar.add(menu4); menuBar.add(menu5);
 * 
 * menu1.add(menu1Item1); menu1.add(menu1Item2); menu1.addSeparator();
 * menu1.add(menu1Item3); menu2.add(menu2Item1); menu2.add(menu2Item2);
 * menu3.add(menu3Item1); menu4.add(menu4Item1); menu5.add(menu5Item1);
 * menu5.add(menu5Item2);
 * 
 * //============================================ //Listeners dos menus
 * //============================================
 * menu1Item1.addActionListener(new SaveListener());
 * menu1Item2.addActionListener(new HTMLExportController.ExportListener());
 * menu4Item1.addActionListener(new TrataEventoBTWiki());
 * menu1Item3.addActionListener(new ActionListener() {
 * 
 * @Override public void actionPerformed(ActionEvent ae) { Object[] options =
 * {Dictionary.getInstance().message(Dictionary.Message.YES),
 * Dictionary.getInstance().message(Dictionary.Message.NO)}; int i =
 * JOptionPane.showOptionDialog(null,
 * Dictionary.getInstance().message(Dictionary.Message.CONFIRM_EXIT),
 * Dictionary.getInstance().message(Dictionary.Message.EXIT),
 * JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
 * options[0]); if (i == JOptionPane.YES_OPTION) { System.exit(0); } else {
 * repaint(); } } }); menu5Item1.addActionListener(new ChangeLanguageToPT());
 * menu5Item2.addActionListener(new ChangeLanguageToEN());
 * 
 * 
 * this.setLayout(new GridLayout(2, 0));
 * 
 * HistoryPanel painelHistorico = new HistoryPanel(); GraficPanel painelGrafico
 * = new GraficPanel();
 * 
 * painelInput = new InputPanel(); Calculator calc = new Calculator(); //
 * WavelengthController inputController = new WavelengthController(calc, //
 * painelInput, painelHistorico);
 * 
 * 
 * JPanel painelCima = new JPanel();
 * 
 * painelCima.add(painelInput, BorderLayout.WEST);
 * painelCima.add(painelHistorico, BorderLayout.EAST);
 * 
 * this.add(painelCima, BorderLayout.CENTER); this.add(painelGrafico,
 * BorderLayout.SOUTH);
 * 
 * 
 * this.setJMenuBar(menuBar);
 * 
 * setVisible(true); }
 * 
 * private void loadSelectedLangButton(){
 * if(Dictionary.getInstance().currentLanguage
 * ().equals(Dictionary.Language.ENGLISH)){ this.menu5Item2.setSelected(true);
 * }else
 * if(Dictionary.getInstance().currentLanguage().equals(Dictionary.Language
 * .PORTUGUESE)){ this.menu5Item1.setSelected(true);} }
 * 
 * private class SaveListener implements ActionListener {
 * 
 * @Override public void actionPerformed(ActionEvent e) {
 * Repository.getInstance().saveData(); } }
 * 
 * private class ChangeLanguageToPT implements ActionListener {
 * 
 * @Override public void actionPerformed(ActionEvent e) {
 * Dictionary.getInstance().changeLanguage(Dictionary.Language.PORTUGUESE);
 * updateFields(); updateInputFields(); } }
 * 
 * private class ChangeLanguageToEN implements ActionListener {
 * 
 * @Override public void actionPerformed(ActionEvent e) {
 * Dictionary.getInstance().changeLanguage(Dictionary.Language.ENGLISH);
 * updateFields(); updateInputFields(); painelInput.getCardOption(2); } }
 * 
 * private void updateFields() {
 * menu1.setText(Dictionary.getInstance().menuName(Dictionary.Menu.FILE));
 * menu2.setText(Dictionary.getInstance().menuName(Dictionary.Menu.VIEW));
 * menu3.setText(Dictionary.getInstance().menuName(Dictionary.Menu.OTHERS));
 * menu4.setText(Dictionary.getInstance().menuName(Dictionary.Menu.HELP));
 * menu5.setText(Dictionary.getInstance().menuName(Dictionary.Menu.LANGUAGE));
 * 
 * //Menu FILE
 * menu1Item1.setText(Dictionary.getInstance().menuName(Dictionary.Menu.SAVE));
 * menu1Item2
 * .setText(Dictionary.getInstance().menuName(Dictionary.Menu.EXPORT_HTML));
 * menu1Item3.setText(Dictionary.getInstance().menuName(Dictionary.Menu.EXIT));
 * //MENU VIEW
 * menu2Item1.setText(Dictionary.getInstance().menuName(Dictionary.Menu.GRAPH));
 * menu2Item2
 * .setText(Dictionary.getInstance().menuName(Dictionary.Menu.CALCULATE_ANGLE));
 * //MENU OTHERS
 * menu3Item1.setText(Dictionary.getInstance().menuName(Dictionary.
 * Menu.CURIOSITIES)); //MENU HELP
 * menu4Item1.setText(Dictionary.getInstance().menuName(Dictionary.Menu.WIKI));
 * //MENU LANGUAGE
 * menu5Item1.setText(Dictionary.getInstance().menuName(Dictionary
 * .Menu.PORTUGUESE));
 * menu5Item2.setText(Dictionary.getInstance().menuName(Dictionary
 * .Menu.ENGLISH));
 * 
 * }
 * 
 * private void updateInputFields() { painelInput.updateFields(); }
 * 
 * class TrataEventoBTWiki implements ActionListener {
 * 
 * @Override public void actionPerformed(ActionEvent e) { Desktop desktop =
 * null; //Primeiro verificamos se Ã© possÃ­vel a integraÃ§Ã£o com o desktop if
 * (!Desktop.isDesktopSupported()) { throw new
 * IllegalStateException("Desktop resources not supported!"); } desktop =
 * Desktop.getDesktop(); //Agora vemos se Ã© possÃ­vel disparar o browser
 * default. if (!desktop.isSupported(Desktop.Action.BROWSE)) { throw new
 * IllegalStateException("No default browser set!"); }
 * 
 * try { URI uri = null; //Pega a URI de um componente de texto. uri = new
 * URI("http://bitbucket.org/ricardo_1111121/fisica_trabalho3_2di/wiki/Home");
 * //Dispara o browser default, que pode ser o Explorer, Firefox ou outro.
 * desktop.browse(uri); } catch (IOException ioe) { ioe.printStackTrace(); }
 * catch (URISyntaxException use) { use.printStackTrace(); } } } }
 */