package View;

import Util.Dictionary;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

/**
* This class builds a jpanel with the input forms
*@author vitorhugo
*/
public class InputUI extends JPanel {
	private final JTextField txt_initial_wavelength_CFW_1;
	private final JTextField txt_initial_wavelength_CFW;
	private final JTextField txt_result_CFW;
	private final JTextField txt_final_wavelength_CIW;
	private final JTextField txt_angle_CIW;
	private final JTextField txt_result_CIW;
	private final CustoButton btn_CFW;
	private final CustoButton btn_CIW;
	private final CustoButton btn_CA;
	private final JLabel lb_name_CFW;
	private final JLabel lb_initial_wavelength_CFW;
	private final JLabel lb_angle_CFW;
	private final JLabel lb_result_CFW;
	private final JLabel lb_final_wavelength_CIW;
	private final JLabel lb_angle_CIW;
	private final JLabel lb_result_CIW;
	private final JLabel lb_initial_wavelength_CA;;
	private final JLabel lb_final_wavelength_CA;
	private final JLabel lb_result_CA;
	private final JLabel lb_name_CA;
	private final JPanel calculate_final_wavelength_panel;
	JTabbedPane tabbedPane;
	private final JTextField txt_name_CFW;
	private final JLabel lb_name_CIW;
	private final JTextField txt_name_CIW;
	private final JTextField txt_initial_wavelength_CA;
	private final JTextField txt_final_wavelength_CA;
	private final JTextField txt_result_CA;
	private final JTextField txt_name_CA;

	/**
	 * Create the panel.
	 */
	public InputUI() {
		setOpaque(false);
		setLayout(null);

		Insets oldInsets = UIManager
				.getInsets("TabbedPane.contentBorderInsets");
		UIManager.put("TabbedPane.contentBorderInsets", new Insets(1, 0, 0, 0));
		UIManager.put("ToolTip.background", Color.WHITE);
		UIManager.put("ToolTip.foreground", Color.DARK_GRAY);
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		tabbedPane.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		tabbedPane.setForeground(Color.DARK_GRAY);
		tabbedPane.setBorder(null);
		// tabbedPane.setBackground(new Color(61, 71, 78));
		UIManager.put("TabbedPane.contentBorderInsets", oldInsets);
		UIManager.put("TabbedPane.tabsOpaque", false);
		UIManager.put("TabbedPane.tabsOverlapBorder", true);
		tabbedPane.setFocusable(false);
		tabbedPane.setBounds(0, 0, 450, 300);

		add(tabbedPane);

		calculate_final_wavelength_panel = new JPanel() {
			@Override
			public void paintComponent(Graphics g)

			{

				g.drawImage(
						Toolkit.getDefaultToolkit().getImage(
								getClass().getResource(
										"/images/inner_panel.jpg")), 0, 0, this);

			}
		};
		calculate_final_wavelength_panel.setForeground(Color.DARK_GRAY);
		calculate_final_wavelength_panel.setOpaque(false);
		// calculate_initial_wavelength_panel.setBorder(null);
		tabbedPane.addTab("Calculate Initial Wavelength", null,
				calculate_final_wavelength_panel, null);
		tabbedPane.setEnabledAt(0, true);

		calculate_final_wavelength_panel.setLayout(null);

		lb_initial_wavelength_CFW = new JLabel();
		lb_initial_wavelength_CFW.setBounds(86, 92, 191, 14);
		calculate_final_wavelength_panel.add(lb_initial_wavelength_CFW);

		txt_initial_wavelength_CFW_1 = new JTextField();
		txt_initial_wavelength_CFW_1.setBounds(290, 89, 86, 20);
		calculate_final_wavelength_panel.add(txt_initial_wavelength_CFW_1);
		txt_initial_wavelength_CFW_1.setColumns(10);

		lb_angle_CFW = new JLabel();
		lb_angle_CFW.setBounds(86, 120, 189, 14);
		calculate_final_wavelength_panel.add(lb_angle_CFW);

		txt_initial_wavelength_CFW = new JTextField();
		txt_initial_wavelength_CFW.setBounds(290, 117, 86, 20);
		calculate_final_wavelength_panel.add(txt_initial_wavelength_CFW);
		txt_initial_wavelength_CFW.setColumns(10);

		lb_result_CFW = new JLabel();
		lb_result_CFW.setBounds(86, 148, 186, 14);
		calculate_final_wavelength_panel.add(lb_result_CFW);

		txt_result_CFW = new JTextField();
		txt_result_CFW.setEditable(false);
		txt_result_CFW.setBounds(290, 145, 86, 20);
		txt_result_CFW.setColumns(10);
		calculate_final_wavelength_panel.add(txt_result_CFW);

		btn_CFW = new CustoButton("");
		btn_CFW.setBounds(287, 183, 89, 23);
		calculate_final_wavelength_panel.add(btn_CFW);

		lb_name_CFW = new JLabel();
		lb_name_CFW.setBounds(86, 61, 191, 14);
		calculate_final_wavelength_panel.add(lb_name_CFW);

		txt_name_CFW = new JTextField();
		txt_name_CFW.setBounds(290, 58, 86, 20);
		calculate_final_wavelength_panel.add(txt_name_CFW);
		txt_name_CFW.setColumns(10);

		JPanel calculate_initial_wavelength_panel = new JPanel() {
			@Override
			public void paintComponent(Graphics g)

			{

				g.drawImage(
						Toolkit.getDefaultToolkit().getImage(
								getClass().getResource(
										"/images/inner_panel.jpg")), 0, 0, this);

			}
		};
		calculate_initial_wavelength_panel.setOpaque(false);
		calculate_initial_wavelength_panel.setLayout(null);
		calculate_initial_wavelength_panel.setBorder(null);
		tabbedPane.addTab("Calculate Final Wavelength", null,
				calculate_initial_wavelength_panel, null);

		lb_final_wavelength_CIW = new JLabel();
		lb_final_wavelength_CIW.setBounds(86, 92, 191, 14);
		calculate_initial_wavelength_panel.add(lb_final_wavelength_CIW);

		txt_final_wavelength_CIW = new JTextField();
		txt_final_wavelength_CIW.setColumns(10);
		txt_final_wavelength_CIW.setBounds(290, 89, 86, 20);
		calculate_initial_wavelength_panel.add(txt_final_wavelength_CIW);

		lb_angle_CIW = new JLabel("Angle");
		lb_angle_CIW.setBounds(86, 120, 189, 14);
		calculate_initial_wavelength_panel.add(lb_angle_CIW);

		txt_angle_CIW = new JTextField();
		txt_angle_CIW.setColumns(10);
		txt_angle_CIW.setBounds(290, 117, 86, 20);
		calculate_initial_wavelength_panel.add(txt_angle_CIW);

		lb_result_CIW = new JLabel();
		lb_result_CIW.setBounds(86, 148, 186, 14);
		calculate_initial_wavelength_panel.add(lb_result_CIW);

		txt_result_CIW = new JTextField();
		txt_result_CIW.setEditable(false);
		txt_result_CIW.setColumns(10);
		txt_result_CIW.setBounds(290, 145, 86, 20);
		calculate_initial_wavelength_panel.add(txt_result_CIW);

		btn_CIW = new CustoButton("");
		btn_CIW.setBounds(287, 183, 89, 23);
		calculate_initial_wavelength_panel.add(btn_CIW);

		lb_name_CIW = new JLabel("");
		lb_name_CIW.setBounds(86, 61, 191, 14);
		calculate_initial_wavelength_panel.add(lb_name_CIW);

		txt_name_CIW = new JTextField();
		txt_name_CIW.setBounds(290, 58, 86, 20);
		calculate_initial_wavelength_panel.add(txt_name_CIW);
		txt_name_CIW.setColumns(10);

		JPanel calculate_angle_panel = new JPanel() {
			@Override
			public void paintComponent(Graphics g)

			{

				g.drawImage(
						Toolkit.getDefaultToolkit().getImage(
								getClass().getResource(
										"/images/inner_panel.jpg")), 0, 0, this);

			}
		};

		calculate_angle_panel.setOpaque(false);
		tabbedPane.addTab("Calculate angle", null, calculate_angle_panel, null);
		calculate_angle_panel.setLayout(null);

		lb_initial_wavelength_CA = new JLabel();
		lb_initial_wavelength_CA.setBounds(86, 92, 191, 14);
		calculate_angle_panel.add(lb_initial_wavelength_CA);

		txt_initial_wavelength_CA = new JTextField();
		txt_initial_wavelength_CA.setColumns(10);
		txt_initial_wavelength_CA.setBounds(290, 89, 86, 20);
		calculate_angle_panel.add(txt_initial_wavelength_CA);

		lb_final_wavelength_CA = new JLabel();
		lb_final_wavelength_CA.setBounds(86, 120, 189, 14);
		calculate_angle_panel.add(lb_final_wavelength_CA);

		txt_final_wavelength_CA = new JTextField();
		txt_final_wavelength_CA.setColumns(10);
		txt_final_wavelength_CA.setBounds(290, 117, 86, 20);
		calculate_angle_panel.add(txt_final_wavelength_CA);

		lb_result_CA = new JLabel();
		lb_result_CA.setBounds(86, 148, 186, 14);
		calculate_angle_panel.add(lb_result_CA);

		txt_result_CA = new JTextField();
		txt_result_CA.setEditable(false);
		txt_result_CA.setColumns(10);
		txt_result_CA.setBounds(290, 145, 86, 20);
		calculate_angle_panel.add(txt_result_CA);

		btn_CA = new CustoButton("");
		btn_CA.setText("Calculate");
		btn_CA.setBounds(287, 183, 89, 23);
		calculate_angle_panel.add(btn_CA);

		lb_name_CA = new JLabel("");
		lb_name_CA.setBounds(86, 61, 191, 14);
		calculate_angle_panel.add(lb_name_CA);

		txt_name_CA = new JTextField();
		txt_name_CA.setColumns(10);
		txt_name_CA.setBounds(290, 58, 86, 20);
		calculate_angle_panel.add(txt_name_CA);

		updateFields();

	}

	public void updateFields() {

		updateTabNames();
		updateButtons();
		updateButtonsImage();
		updateLabels();
		updateToolTipText();

	}

	private void updateButtons() {
		btn_CFW.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.CALCULATE));
		btn_CIW.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.CALCULATE));
		btn_CA.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.CALCULATE));
	}

	private void updateLabels() {
		// name
		lb_name_CFW.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.NAME));
		lb_name_CIW.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.NAME));
		lb_name_CA.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.NAME));

		// angle
		lb_angle_CFW.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.ANGLE));
		lb_angle_CIW.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.ANGLE));

		// initial wavelength
		lb_initial_wavelength_CFW.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.INITIAL_WAVELENGTH));
		lb_initial_wavelength_CA.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.INITIAL_WAVELENGTH));

		// final wavelength
		lb_final_wavelength_CIW.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.FINAL_WAVELENGTH));
		lb_final_wavelength_CA.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.FINAL_WAVELENGTH));

		// result
		lb_result_CFW.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.RESULT));
		lb_result_CIW.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.RESULT));
		lb_result_CA.setText(Dictionary.getInstance().physic(
				Dictionary.Physic.RESULT));
	}

	private void updateButtonsImage() {
		btn_CFW.update(
				Dictionary.getInstance().physic(Dictionary.Physic.CALCULATE)
						+ ".png",
				Dictionary.getInstance().physic(Dictionary.Physic.CALCULATE)
						+ "_roll.png");
		btn_CIW.update(
				Dictionary.getInstance().physic(Dictionary.Physic.CALCULATE)
						+ ".png",
				Dictionary.getInstance().physic(Dictionary.Physic.CALCULATE)
						+ "_roll.png");
		btn_CA.update(
				Dictionary.getInstance().physic(Dictionary.Physic.CALCULATE)
						+ ".png",
				Dictionary.getInstance().physic(Dictionary.Physic.CALCULATE)
						+ "_roll.png");
	}

	private void updateTabNames() {
		tabbedPane.setTitleAt(
				0,
				Dictionary.getInstance().menuName(
						Dictionary.Menu.CALCULATE_FINAL_WV));

		tabbedPane.setTitleAt(
				1,
				Dictionary.getInstance().menuName(
						Dictionary.Menu.CALCULATE_INITIAL_WL));

		tabbedPane.setTitleAt(
				2,
				Dictionary.getInstance().menuName(
						Dictionary.Menu.CALCULATE_ANGLE));

	}

	private void updateToolTipText() {

		txt_name_CFW.setToolTipText(Dictionary.getInstance().physic(
				Dictionary.Physic.NAME_TOOLTIP));
		txt_name_CIW.setToolTipText(Dictionary.getInstance().physic(
				Dictionary.Physic.NAME_TOOLTIP));
		txt_initial_wavelength_CFW_1.setToolTipText(Dictionary.getInstance()
				.physic(Dictionary.Physic.INITIAL_WAVELENGTH_TOOLTIP));
		txt_initial_wavelength_CFW.setToolTipText(Dictionary.getInstance()
				.physic(Dictionary.Physic.ANGLE_TOOLTIP));
		txt_final_wavelength_CIW.setToolTipText(Dictionary.getInstance()
				.physic(Dictionary.Physic.FINAL_WAVELENGTH_TOOLTIP));
		txt_angle_CIW.setToolTipText(Dictionary.getInstance().physic(
				Dictionary.Physic.ANGLE_TOOLTIP));
		txt_initial_wavelength_CA.setToolTipText(Dictionary.getInstance()
				.physic(Dictionary.Physic.NAME_TOOLTIP));
		txt_name_CA.setToolTipText(Dictionary.getInstance().physic(
				Dictionary.Physic.INITIAL_WAVELENGTH_TOOLTIP));
		txt_final_wavelength_CA.setToolTipText(Dictionary.getInstance().physic(
				Dictionary.Physic.FINAL_WAVELENGTH_TOOLTIP));
	}

	public String getNameCFW() {
		return txt_name_CFW.getText();
	}

	public double getInitialWavelengthCFW() {

		return Double.parseDouble(txt_initial_wavelength_CFW_1.getText());

	}

	public double getAngleCFW() {

		return Double.parseDouble(txt_initial_wavelength_CFW.getText());

	}

	public double getResultCFW() {

		return Double.parseDouble(txt_result_CFW.getText());

	}

	public void setResultCFW(String solution) {

		txt_result_CFW.setText(solution);

	}

	public String getNameCIW() {
		return txt_name_CIW.getText();
	}

	public double getFinalWavelengthCIW() {
		return Double.parseDouble(txt_final_wavelength_CIW.getText());
	}

	public double getAngleCIW() {
		return Double.parseDouble(txt_angle_CIW.getText());
	}

	public double getResultCIW() {
		return Double.parseDouble(txt_result_CIW.getText());
	}

	public void setResultCIW(String solution) {
		txt_result_CIW.setText(solution);
	}

	public String getNameCA() {
		return txt_name_CA.getText();
	}

	public Double getInitialWavelengthCA() {
		return Double.parseDouble(txt_initial_wavelength_CA.getText());
	}

	public Double getFinalWavelengthCA() {
		return Double.parseDouble(txt_final_wavelength_CA.getText());
	}

	public void setResultCA(String solution) {
		txt_result_CA.setText(solution);
	}

	// If the calculateButton is clicked execute a method
	// in the Controller named actionPerformed
	public void addCalculateFinalWavelengthListener(
			ActionListener listenForCalcButton) {

		btn_CFW.addActionListener(listenForCalcButton);
	}

	public void addCalculateInitialWavelengthListener(
			ActionListener listenForCalcButton) {

		btn_CIW.addActionListener(listenForCalcButton);
	}

	public void addCalculateAngleListener(ActionListener listenForCalcButton) {
		btn_CA.addActionListener(listenForCalcButton);
	}

	// Open a popup that contains the error message passed
	public void displayErrorMessage(String errorMessage) {

		JOptionPane.showMessageDialog(
				this,
				errorMessage,
				"Error",
				JOptionPane.ERROR_MESSAGE,
				new ImageIcon(Toolkit.getDefaultToolkit().getImage(
						getClass().getResource("/images/error.png"))));

	}

}
