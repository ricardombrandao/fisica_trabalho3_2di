/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Calculation;
import Persistence.Repository;
import java.awt.Color;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * This class builds the history jpanel
 * @author Scare
 */
public class HistoryPanel extends JPanel {

    private JList list;
    private JScrollPane scroll;

    public HistoryPanel() {
        super();
        this.setBackground(Color.GREEN);
        initComponents();
    }

    private void initComponents() {
        list = new JList(Repository.getInstance().getList().toArray());
        scroll = new JScrollPane();
        scroll.setViewportView(list);
        this.add(scroll);
    }

    public void updateList() {
        DefaultListModel listElements;
        listElements = new DefaultListModel<Calculation>();
        for (Calculation calculation : Repository.getInstance().getList()) {
            listElements.addElement(calculation);
        }
        list.setModel(listElements);
        list.setSelectedIndex(0);
    }
}
