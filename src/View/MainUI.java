package View;

import Controller.HTMLExportController;
import Controller.WavelengthController;
import Model.Calculation;
import Model.Calculator;
import Persistence.Repository;
import Util.Dictionary;
import com.jogamp.opengl.util.FPSAnimator;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

/**
 * This class displays the main frame and loads all the other components.
 *
 * @author vitorhugo
 */
public class MainUI extends JFrame {

    /**
     * Launch the application.
     */
    public static void main(String[] args) {

        Repository.getInstance().loadData();

        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
                .getInstalledLookAndFeels()) {
            if ("Windows".equals(info.getName())) {
                try {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {

                    e.printStackTrace();
                }
                break;
            }
        }

        UIManager.put("Button.font", new Font("Segoe UI", Font.PLAIN, 12));
        UIManager.put("Label.font", new Font("Segoe UI", Font.PLAIN, 12));
        UIManager.put("List.font", new Font("Segoe UI", Font.PLAIN, 12));
        UIManager.put("Menu.font", new Font("Segoe UI", Font.PLAIN, 12));
        UIManager.put("MenuBar.font", new Font("Segoe UI", Font.PLAIN, 12));
        UIManager.put("MenuItem.font", new Font("Segoe UI", Font.PLAIN, 12));
        UIManager.put("Panel.font", new Font("Segoe UI", Font.PLAIN, 12));
        UIManager.put("TextField.font", new Font("Segoe UI", Font.PLAIN, 12));
        UIManager.put("TabbedPane.font", new Font("Segoe UI", Font.BOLD, 12));
        UIManager.put("Menu.selectionBackground", new Color(245, 29, 29));
        UIManager.put("Menu.selectionForeground", Color.white);
        UIManager.put("Menu.foreground", Color.white);
        UIManager.put("MenuItem.foreground", Color.white);
        UIManager.put("MenuItem.background", new Color(53, 95, 130));
        UIManager.put("MenuItem.selectionBackground", new Color(245, 29, 29));

        UIManager.put("MenuItem.selectionForeground", Color.white);

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    MainUI frame = new MainUI();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    @SuppressWarnings({"serial", "rawtypes"})
    public MainUI() {
        setBackground(new Color(79, 104, 128));

        setVisible(true);
        setPreferredSize(new Dimension(800, 600));
        setTitle("Compton Simulation");
        setSize(new Dimension(767, 600));
        setMaximumSize(new Dimension(800, 600));
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// setBounds(100, 100, 450, 300);

        // cardLayout = new CardLayout(0, 0);
        calculations_panel = new InputUI();
        calculations_panel.setBounds(10, 11, 450, 274);
        calculations_panel.setBorder(null);

        JMenuBar menuBar = new JMenuBar() {

            @Override
            public void paintComponent(Graphics g) {

                g.drawImage(
                        Toolkit.getDefaultToolkit().getImage(
                                getClass().getResource("/images/menu_bar.jpg")),
                        0, 0, this);

            }

        };

        menuBar.add(Box.createRigidArea(new Dimension(0, 25)));
        menuBar.setBorder(null);
        menuBar.setBorderPainted(false);
        setJMenuBar(menuBar);

        Dictionary.getInstance().changeLanguage(Dictionary.Language.ENGLISH);

        menu1 = new JMenu("              "
                + Dictionary.getInstance().menuName(Dictionary.Menu.FILE));
        menu1.setForeground(Color.DARK_GRAY);
        menu2 = new JMenu(Dictionary.getInstance().menuName(
                Dictionary.Menu.VIEW));
        menu2.setForeground(Color.DARK_GRAY);
        menu3 = new JMenu(Dictionary.getInstance().menuName(
                Dictionary.Menu.OTHERS));
        menu3.setForeground(Color.DARK_GRAY);
        menu4 = new JMenu(Dictionary.getInstance().menuName(
                Dictionary.Menu.HELP));
        menu4.setForeground(Color.DARK_GRAY);
        menu5 = new JMenu(Dictionary.getInstance().menuName(
                Dictionary.Menu.LANGUAGE));
        menu5.setForeground(Color.DARK_GRAY);

        // Menu FILE
        menu1Item1 = new JMenuItem(Dictionary.getInstance().menuName(
                Dictionary.Menu.SAVE));
        menu1Item1.setForeground(Color.DARK_GRAY);
        menu1Item2 = new JMenuItem(Dictionary.getInstance().menuName(
                Dictionary.Menu.EXPORT_HTML));
        menu1Item2.setForeground(Color.DARK_GRAY);
        menu1Item3 = new JMenuItem(Dictionary.getInstance().menuName(
                Dictionary.Menu.EXIT));
        menu1Item3.setForeground(Color.DARK_GRAY);
        // MENU VIEW
        menu2Item1 = new JMenuItem(Dictionary.getInstance().menuName(
                Dictionary.Menu.GRAPH));
        menu2Item1.setForeground(Color.DARK_GRAY);
        menu2Item2 = new JMenuItem(Dictionary.getInstance().menuName(
                Dictionary.Menu.SIMULATION));
        menu2Item2.setForeground(Color.DARK_GRAY);
        // MENU OTHERS
        menu3Item1 = new JMenuItem(Dictionary.getInstance().menuName(
                Dictionary.Menu.BIBLIOGRAPHY));
        menu3Item1.setForeground(Color.DARK_GRAY);
        // MENU HELP
        menu4Item1 = new JMenuItem(Dictionary.getInstance().menuName(
                Dictionary.Menu.WIKI));
        menu4Item1.setForeground(Color.DARK_GRAY);
        // MENU LANGUAGE
        ButtonGroup grupoBotoes = new ButtonGroup();
        JRadioButtonMenuItem lang1 = new JRadioButtonMenuItem(Dictionary
                .getInstance().menuName(Dictionary.Menu.PORTUGUESE),
                new ImageIcon(Toolkit.getDefaultToolkit().getImage(
                                getClass().getResource("/images/PT.gif"))));
        JRadioButtonMenuItem lang2 = new JRadioButtonMenuItem(Dictionary
                .getInstance().menuName(Dictionary.Menu.ENGLISH),
                new ImageIcon(Toolkit.getDefaultToolkit().getImage(
                                getClass().getResource("/images/ENG.gif"))));
        grupoBotoes.add(lang1);
        grupoBotoes.add(lang2);
        menu5Item1 = lang1;
        menu5Item2 = lang2;
        loadSelectedLangButton();

        menuBar.add(menu1);
        menuBar.add(menu2);
        menuBar.add(menu3);
        menuBar.add(menu4);
        menuBar.add(menu5);
        menuBar.setBackground(new Color(227, 229, 232));
        menuBar.setOpaque(false);

        menu1.add(menu1Item1);
        menu1.add(menu1Item2);
        menu1.addSeparator();
        menu1.add(menu1Item3);
        menu2.add(menu2Item1);
        menu2.add(menu2Item2);
        menu3.add(menu3Item1);
        menu4.add(menu4Item1);
        menu5.add(menu5Item1);
        menu5.add(menu5Item2);

		// ============================================
        // Listeners dos menus
        // ============================================
        menu1Item1.addActionListener(new SaveListener());
        menu1Item2.addActionListener(new HTMLExportController.ExportListener());
        menu2Item1.addActionListener(new Graph());
        menu2Item2.addActionListener(new Simulation());
        menu4Item1.addActionListener(new TrataEventoBTWiki());
        menu1Item3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Object[] options = {
                    Dictionary.getInstance()
                    .message(Dictionary.Message.YES),
                    Dictionary.getInstance().message(Dictionary.Message.NO)};
                int i = JOptionPane
                        .showOptionDialog(
                                null,
                                Dictionary.getInstance().message(
                                        Dictionary.Message.CONFIRM_EXIT),
                                Dictionary.getInstance().message(
                                        Dictionary.Message.EXIT),
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null, options,
                                options[0]);
                if (i == JOptionPane.YES_OPTION) {
                    System.exit(0);
                } else {
                    repaint();
                }
            }
        });
        menu5Item1.addActionListener(new ChangeLanguageToPT());
        menu5Item2.addActionListener(new ChangeLanguageToEN());
        menu3Item1.addActionListener(new Bibliography());
        contentPane = new JPanel();
        contentPane.setBackground(new Color(79, 104, 128));
        contentPane.setOpaque(false);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);

        JPanel history_panel = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                g.drawImage(
                        Toolkit.getDefaultToolkit().getImage(
                                getClass().getResource(
                                        "/images/history_panel.jpg")), 0, 0,
                        null);
                repaint();
            }
        };

        history_panel.setBounds(470, 11, 273, 274);
        history_panel.setOpaque(false);

        JPanel graph_panel = new JPanel();
        graph_panel.setBounds(10, 296, 733, 237);
        graph_panel.setBorder(null);

        Calculator calc = new Calculator();
        WavelengthController inputController = new WavelengthController(calc,
                calculations_panel, this);
        contentPane.setLayout(null);
        contentPane.add(graph_panel);
        graph_panel.setLayout(null);
        contentPane.add(calculations_panel);
        contentPane.add(history_panel);
        history_panel.setLayout(null);

        scrollPane = new JScrollPane();
        scrollPane.setFont(new Font("Segoe UI", Font.PLAIN, 11));
        scrollPane.setBorder(null);
        scrollPane.setBounds(43, 64, 194, 168);
        scrollPane.setOpaque(false);
        scrollPane.setBackground(Color.white);
        history_panel.add(scrollPane);

        list = new JList(Repository.getInstance().last5Results());
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectionBackground(new Color(79, 104, 128));
        list.setFont(new Font("Segoe UI", Font.PLAIN, 11));

        scrollPane.setViewportView(list);
        list.setBackground(Color.white);

        list.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                JList l = (JList) e.getSource();
                ListModel m = l.getModel();
                int index = l.locationToIndex(e.getPoint());
                if (index > -1) {
                    l.setToolTipText(((Calculation) m.getElementAt(index))
                            .toolTipText());
                }
            }
        });

        history_label = new JLabel(Dictionary.getInstance().physic(
                Dictionary.Physic.HISTORY));
        history_label.setForeground(Color.DARK_GRAY);
        history_label.setFont(new Font("Segoe UI", Font.PLAIN, 11));
        history_label.setBounds(43, 23, 194, 14);
        history_panel.add(history_label);
    }

    private void loadSelectedLangButton() {
        if (Dictionary.getInstance().currentLanguage()
                .equals(Dictionary.Language.ENGLISH)) {
            this.menu5Item2.setSelected(true);
        } else if (Dictionary.getInstance().currentLanguage()
                .equals(Dictionary.Language.PORTUGUESE)) {
            this.menu5Item1.setSelected(true);
        }
    }

    private class SaveListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Repository.getInstance().saveData();
            JOptionPane.showMessageDialog(null, "", "",
                    JOptionPane.INFORMATION_MESSAGE, null);
        }
    }

    private class Bibliography implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Bibliography test = new Bibliography();
        }
    }

    private class Graph implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            ArrayList names = new ArrayList<String>();
            ArrayList results = new ArrayList<Double>();
            ArrayList list = new ArrayList<Calculation>();

            for (Calculation calc : Repository.getInstance().getList()) {
                names.add(calc.get_calculation_name());
                results.add(calc.get_calculation_result());
            }

            Chart chart = new Chart("Comparative Analysis", names, results);

        }
    }

    private class Simulation implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
           JOGL2Setup_GLCanvas jogl2= new JOGL2Setup_GLCanvas();
           jogl2.setFrameVisible();
        }
    }

    private class ChangeLanguageToPT implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Dictionary.getInstance().changeLanguage(
                    Dictionary.Language.PORTUGUESE);
            updateFields();
            updateInputFields();
        }
    }

    private class ChangeLanguageToEN implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            Dictionary.getInstance()
                    .changeLanguage(Dictionary.Language.ENGLISH);
            updateFields();
            updateInputFields();

        }
    }

    public void errorMessage(String message, String title) {
        JOptionPane.showMessageDialog(
                this,
                message,
                title,
                JOptionPane.ERROR_MESSAGE,
                new ImageIcon(Toolkit.getDefaultToolkit().getImage(
                                getClass().getResource("/images/inner_panel.jpg"))));
    }

    private void updateFields() {
        menu1.setText("              "
                + Dictionary.getInstance().menuName(Dictionary.Menu.FILE));
        menu2.setText(Dictionary.getInstance().menuName(Dictionary.Menu.VIEW));
        menu3.setText(Dictionary.getInstance().menuName(Dictionary.Menu.OTHERS));
        menu4.setText(Dictionary.getInstance().menuName(Dictionary.Menu.HELP));
        menu5.setText(Dictionary.getInstance().menuName(
                Dictionary.Menu.LANGUAGE));

        // Menu FILE
        menu1Item1.setText(Dictionary.getInstance().menuName(
                Dictionary.Menu.SAVE));
        menu1Item2.setText(Dictionary.getInstance().menuName(
                Dictionary.Menu.EXPORT_HTML));
        menu1Item3.setText(Dictionary.getInstance().menuName(
                Dictionary.Menu.EXIT));
        // MENU VIEW
        menu2Item1.setText(Dictionary.getInstance().menuName(
                Dictionary.Menu.GRAPH));
        menu2Item2.setText(Dictionary.getInstance().menuName(
                Dictionary.Menu.SIMULATION));
        // MENU OTHERS
        menu3Item1.setText(Dictionary.getInstance().menuName(
                Dictionary.Menu.BIBLIOGRAPHY));
        // MENU HELP
        menu4Item1.setText(Dictionary.getInstance().menuName(
                Dictionary.Menu.WIKI));
        // MENU LANGUAGE
        menu5Item1.setText(Dictionary.getInstance().menuName(
                Dictionary.Menu.PORTUGUESE));
        menu5Item2.setText(Dictionary.getInstance().menuName(
                Dictionary.Menu.ENGLISH));

        history_label.setText(Dictionary.getInstance().physic(
                Dictionary.Physic.HISTORY));
    }

    private void updateInputFields() {
        calculations_panel.updateFields();
    }

    class TrataEventoBTWiki implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Desktop desktop = null;
            // Primeiro verificamos se Ã© possÃ­vel a integraÃ§Ã£o com o desktop
            if (!Desktop.isDesktopSupported()) {
                throw new IllegalStateException(
                        "Desktop resources not supported!");
            }
            desktop = Desktop.getDesktop();
            // Agora vemos se Ã© possÃ­vel disparar o browser default.
            if (!desktop.isSupported(Desktop.Action.BROWSE)) {
                throw new IllegalStateException("No default browser set!");
            }

            try {
                URI uri = null;
                // Pega a URI de um componente de texto.
                uri = new URI(
                        "http://bitbucket.org/ricardo_1111121/fisica_trabalho3_2di/wiki/Home");
				// Dispara o browser default, que pode ser o Explorer, Firefox
                // ou outro.
                desktop.browse(uri);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } catch (URISyntaxException use) {
                use.printStackTrace();
            }
        }
    }

    public void updateList() {
        DefaultListModel listElements;
        listElements = new DefaultListModel<Calculation>();
        for (Calculation calculation : Repository.getInstance().getList()) {
            listElements.addElement(calculation);
        }
        list.setModel(listElements);
        list.setSelectedIndex(0);
    }

    private final JPanel contentPane;
    private final InputUI calculations_panel;
    JLabel history_label;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuEdit;
    private javax.swing.JMenu menuFile;
    private final JMenu menu1;
    private final JMenu menu2;
    private final JMenu menu3;
    private final JMenu menu4;
    private final JMenu menu5;
    // Menu FILE
    private final JMenuItem menu1Item1;
    private final JMenuItem menu1Item2;
    private final JMenuItem menu1Item3;
    // MENU VIEW
    private final JMenuItem menu2Item1;
    private final JMenuItem menu2Item2;
    // MENU OTHERS
    private final JMenuItem menu3Item1;
    // MENU HELP
    private final JMenuItem menu4Item1;
    // MENU LANGUAGE
    private final JMenuItem menu5Item1;
    private final JMenuItem menu5Item2;
    private final JScrollPane scrollPane;
    private final JList list;
}
