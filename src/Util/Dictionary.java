package Util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This is a singleton class responsible for the language and translations of
 * the application.
 * @author VitorHugo
 */
public class Dictionary {

    private static Dictionary instance;
    private Language language;

    /**
     * Enumerator with all the menus available on this application
     */
    public enum Menu {

        FILE("File", "Ficheiro"), VIEW("View", "Ver"), OTHERS("Others", "Outros"), HELP("Help", "Ajuda"),
        SAVE("Save", "Gravar"), LOAD("Load", "Carregar"), EXPORT_HTML("Export as HTML", "Exportar como HTML"), EXIT("Exit", "Sair"),
        CALCULATE_INITIAL_WL("Calculate initial wavelength", "Calcular comprimento de onda inicial"),CALCULATE_ANGLE("Calculate angle", "Calcular angulo"),CALCULATE_FINAL_WV("Calculate final wavelength", "Calcular comprimento de onda final"), GRAPH("Graph", "Grafico"),
        BIBLIOGRAPHY("Bibliography", "Bibliografia"),
        WIKI("Wiki", "Wiki"),
        LANGUAGE("Language", "Lingua"),
        ENGLISH("English", "Ingles"),
        PORTUGUESE("Portuguese", "Portugues"),
        SIMULATION("Simulation","Simulação");

        private String english_string;
        private String portuguese_string;

        private Menu(String english_string, String portuguese_string) {
            this.english_string = english_string;
            this.portuguese_string = portuguese_string;
        }

        private String english() {
            return english_string;
        }

        private String portuguese() {
            return portuguese_string;
        }

    }

    /**
     * Enumerator with all the available messages on this application
     */
    public enum Message {

        CONFIRM_EXIT("Are you sure you want to exit?", "Tem a certeza que deseja sair?"),
        EXIT("Exit", "Saida"),
        YES("Yes", "Sim"),
        NO("No", "Nao");

        private String english_string;
        private String portuguese_string;

        private Message(String english_string, String portuguese_string) {
            this.english_string = english_string;
            this.portuguese_string = portuguese_string;
        }

        private String english() {
            return english_string;
        }

        private String portuguese() {
            return portuguese_string;
        }

    }

    /**
     * Enumerator with physic terms
     */
    public enum Physic {

        WAVELENGTH("Wavelength", "Comprimento de onda"),
        ANGLE("Angle", "Angulo"),
 FINAL_WAVELENGTH("Final wavelength",
				"Comprimento de onda final"), INITIAL_WAVELENGTH(
				"Initial wavelength", "Comprimento de onda inicial"),
        RESULT("Result", "Resultado"),
        HISTORY("History", "Historico"),
        CALCULATE("Calculate", "Calcular"),
 DATE("Date", "Data"), NAME("Name",
				"Nome"), NAME_TOOLTIP("Insert the simulation's name",
				"Insira o nome da simulação"), ANGLE_TOOLTIP(
				"Insert the angle", "Insira o ângulo"), INITIAL_WAVELENGTH_TOOLTIP(
				"Insert the initial wavelength",
				"Insira o comprimento de onda inicial"), FINAL_WAVELENGTH_TOOLTIP(
				"Insert the final wavelength",
				"Insira o comprimento de onda final");
		;

        private String english_string;
        private String portuguese_string;

        private Physic(String english_string, String portuguese_string) {
            this.english_string = english_string;
            this.portuguese_string = portuguese_string;
        }

        private String english() {
            return english_string;
        }

        private String portuguese() {
            return portuguese_string;
        }

    }

    /**
     * Enumerator for the available languages of this application
     */
    public enum Language {

        PORTUGUESE("Portuguese", "Portugues", "PT"),
        ENGLISH("English", "Ingles", "ENG");

        private String english_string;
        private String portuguese_string;
        private String short_form;

        private Language(String english_string, String portuguese_string, String short_form) {
            this.english_string = english_string;
            this.portuguese_string = portuguese_string;
            this.short_form = short_form;
        }

        private String english() {
            return english_string;
        }

        private String portuguese() {
            return portuguese_string;
        }

        private String short_form() {
            return short_form;
        }

        @Override
        public String toString() {
            return short_form;
        }

    }
    
    /**
     * Class Constructor The default language is set to english
     */
    protected Dictionary() {
        language = Language.ENGLISH;
    }

    /**
     * Returns an instance of the class
     *
     * @return an instance of the class
     */
    public static Dictionary getInstance() {

        if (instance == null) {
            instance = new Dictionary();
        }

        return instance;
    }

    /**
     * Changes the application language
     *
     * @param language a enum
     */
    public void changeLanguage(Language language) {
        this.language = language;
    }

    /**
     * Returns the menu translated to the current application language
     *
     * @param menu menu enumerator value
     * @return menu as a string translated to the current application language
     */
    public String menuName(Menu menu) {
        return language.equals(Dictionary.Language.PORTUGUESE) ? menu.portuguese() : menu.english();
    }

    /**
     * Returns the message translated to the current application language
     *
     * @param message message enumerator value
     * @return message as a string translated to the current application
     * language
     */
    public String message(Message message) {
        return language.equals(Dictionary.Language.PORTUGUESE) ? message.portuguese() : message.english();
    }

    /**
     * Returns the physic term translated to the current application language
     *
     * @param physic physic enumerator value
     * @return physic term as a string translated to the current application
     * language
     */
    public String physic(Physic physic) {
        return language.equals(Dictionary.Language.PORTUGUESE) ? physic.portuguese() : physic.english();
    }

    /**
     * Returns the language being used
     *
     * @return the language being used
     */
    public Language currentLanguage() {
        return language;
    }
    

}
