/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import Model.AngleCalculation;
import Model.Calculation;
import Model.FinalWavelengthCalculation;
import Model.InitialWavelengthCalculation;
import Persistence.Repository;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;

/**
 * This class is responsible for exporting data to HTML.
 * @author vitorhugo
 */
public class HTMLExport {

    private String content;
    private String structure;
    private String css;
    private String content_CA;
    private String content_CIW;
    private String content_CFW;
    private DecimalFormat df = new DecimalFormat("0.000E0");
    private Dictionary dictionary = Dictionary.getInstance();

	/**
	* Generates the html structure of the file.
	*/
    private void generateStructure() {

        structure = "<!DOCTYPE html>\n"
                + "<html lang=\"en\">\n"
                + "<head>\n"
                + "  <meta charset=\"utf-8\">\n"
                + "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "  <meta name=\"description\" content=\"\">\n"
                + "  <meta name=\"author\" content=\"\">\n"
                + "  <link rel=\"shortcut icon\" href=\"docs-assets/ico/favicon.png\">\n"
                + "\n"
                + "  <title>Sticky Footer Template for Bootstrap</title>\n"
                + "\n"
                + "  <!-- Bootstrap core CSS -->\n"
                + "  <link href=\"css/bootstrap.css\" rel=\"stylesheet\">\n"
                + "\n"
                + "  <!-- Custom styles for this template -->\n"
                + "  <link href=\"sticky-footer.css\" rel=\"stylesheet\">\n"
                + "\n"
                + "  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->\n"
                + "    <!--[if lt IE 9]>\n"
                + "      <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>\n"
                + "      <script src=\"https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js\"></script>\n"
                + "      <![endif]-->\n"
                + "    </head>\n"
                + "\n"
                + "    <body>\n"
                + "\n"
                + "      <!-- Wrap all page content here -->\n"
                + "      <div id=\"wrap\">\n"
                + "\n"
                + "        <!-- Begin page content -->\n"
                + "        <div class=\"container\">\n"
                + "          <div class=\"page-header\">"
                + content
                + "  </div> <!-- page-header -->\n"
                + "\n"
                + "\n"
                + "        </div> <!-- container -->\n"
                + "       </div> <!-- wrap -->\n"
                + "\n"
                + "\n"
                + "\n"
                + "    <!-- Bootstrap core JavaScript\n"
                + "    ================================================== -->\n"
                + "    <!-- Placed at the end of the document so the pages load faster -->\n"
                + "  </body>\n"
                + "  </html>";
    }
	
	/*
	* Generates all tables
	*/
    private void generateContent() {
        content = "";

        content += Repository.getInstance().hasFinalWavelengthCalculations() ? generateContentCFW() : "";
        content += Repository.getInstance().hasInitialWavelengthCalculations() ? generateContentCIW() : "";
        content += Repository.getInstance().hasInitialWavelengthCalculations() ? generateContentCA() : "";
    }
	
	/*
	* Generates angle calculations table
	*/
    private String generateContentCA() {

        String title = dictionary.menuName(Dictionary.Menu.CALCULATE_ANGLE);
        String header1 = "#";
        String header2 = dictionary.physic(Dictionary.Physic.NAME);;
        String header3 = "&#955;";
        String header4 = "&#955;'";
        String header5 = "&#952;";
        String table_rows = "";
        int index = 0;
        AngleCalculation angle_calculation;

        for (Calculation calculation : Repository.getInstance().getList()) {

            if (calculation instanceof AngleCalculation) {

                angle_calculation = (AngleCalculation) calculation;
                index++;

                table_rows += "              <tr>\n";
                table_rows += "                <td>" + index + "</td>\n";
                table_rows += "                <td>" + angle_calculation.get_calculation_name() + "</td>\n";
                table_rows += "                <td>" + angle_calculation.initialWavelengthToString() + "</td>\n";
                table_rows += "                <td>" + angle_calculation.finalWavelengthToString() + "</td>\n";
                table_rows += "                <td>" + angle_calculation.angleToString() + "</td>\n";
                table_rows += "              </tr>\n";

            }
        }

        content_CA += "<h1>" + title + "</h1>\n"
                + "\n"
                + "            <div class=\"panel panel-primary\">\n"
                + "            <!-- Default panel contents -->\n"
                + "            <div class=\"panel-heading\">" + title + "</div>\n"
                + "\n"
                + "            <!-- Table -->\n"
                + "            <table class=\"table\">\n"
                + "             <thead>\n"
                + "              <tr>\n"
                + "                <th>" + header1 + "</th>\n"
                + "                <th>" + header2 + "</th>\n"
                + "                <th>" + header3 + "</th>\n"
                + "                <th>" + header4 + "</th>\n"
                + "              </tr>\n"
                + "            </thead>\n"
                + "            <tbody>\n"
                + table_rows
                + "            </tbody>\n"
                + "          </table>\n"
                + "        </div><!-- panel panel-primary -->";


        return content_CA;
    }

	/*
	* Generates initial wavelength calculations table
	*/
    private String generateContentCIW() {

        String title = dictionary.menuName(Dictionary.Menu.CALCULATE_INITIAL_WL);
        String header1 = "#";
        String header2 = dictionary.physic(Dictionary.Physic.NAME);
        String header3 = "&#955;'";
        String header4 = "&#952;";
        String header5 = "&#955;";
        String table_rows = "";
        int index = 0;
        InitialWavelengthCalculation initial_wavelength_calculation;

        for (Calculation calculation : Repository.getInstance().getList()) {

            if (calculation instanceof InitialWavelengthCalculation) {

                initial_wavelength_calculation = (InitialWavelengthCalculation) calculation;
                index++;

                table_rows += "              <tr>\n";
                table_rows += "                <td>" + index + "</td>\n";
                table_rows += "                <td>" + initial_wavelength_calculation.get_calculation_name() + "</td>\n";
                table_rows += "                <td>" + initial_wavelength_calculation.finalWavelengthToString() + "</td>\n";
                table_rows += "                <td>" + initial_wavelength_calculation.angleToString() + "</td>\n";
                table_rows += "                <td>" + initial_wavelength_calculation.initialWavelengthToString() + "</td>\n";
                table_rows += "              </tr>\n";

            }
        }

        content_CIW += "<h1>" + title + "</h1>\n"
                + "\n"
                + "            <div class=\"panel panel-primary\">\n"
                + "            <!-- Default panel contents -->\n"
                + "            <div class=\"panel-heading\">" + title + "</div>\n"
                + "\n"
                + "            <!-- Table -->\n"
                + "            <table class=\"table\">\n"
                + "             <thead>\n"
                + "              <tr>\n"
                + "                <th>" + header1 + "</th>\n"
                + "                <th>" + header2 + "</th>\n"
                + "                <th>" + header3 + "</th>\n"
                + "                <th>" + header4 + "</th>\n"
                + "              </tr>\n"
                + "            </thead>\n"
                + "            <tbody>\n"
                + table_rows
                + "            </tbody>\n"
                + "          </table>\n"
                + "        </div><!-- panel panel-primary -->";

        return content_CIW;

    }

	/*
	* Generates final wavelength calculations table
	*/
    private String generateContentCFW() {

        String title = dictionary.menuName(Dictionary.Menu.CALCULATE_FINAL_WV);
        String header1 = "#";
        String header2 = dictionary.physic(Dictionary.Physic.NAME);
        String header3 = "&#955;";
        String header4 = "&#952;";
        String header5 = "&#955;'";
        String table_rows = "";
        int index = 0;
        FinalWavelengthCalculation final_wavelength_calculation;

        for (Calculation calculation : Repository.getInstance().getList()) {

            if (calculation instanceof FinalWavelengthCalculation) {

                final_wavelength_calculation = (FinalWavelengthCalculation) calculation;
                index++;

                table_rows += "              <tr>\n";
                table_rows += "                <td>" + index + "</td>\n";
                table_rows += "                <td>" + final_wavelength_calculation.get_calculation_name() + "</td>\n";
                table_rows += "                <td>" + final_wavelength_calculation.finalWavelengthToString() + "</td>\n";
                table_rows += "                <td>" + final_wavelength_calculation.angleToString() + "</td>\n";
                table_rows += "                <td>" + final_wavelength_calculation.initialWavelengthToString() + "</td>\n";
                table_rows += "              </tr>\n";

            }
        }

        content_CFW += "<h1>" + title + "</h1>\n"
                + "\n"
                + "            <div class=\"panel panel-primary\">\n"
                + "            <!-- Default panel contents -->\n"
                + "            <div class=\"panel-heading\">" + title + "</div>\n"
                + "\n"
                + "            <!-- Table -->\n"
                + "            <table class=\"table\">\n"
                + "             <thead>\n"
                + "              <tr>\n"
                + "                <th>" + header1 + "</th>\n"
                + "                <th>" + header2 + "</th>\n"
                + "                <th>" + header3 + "</th>\n"
                + "                <th>" + header4 + "</th>\n"
                + "              </tr>\n"
                + "            </thead>\n"
                + "            <tbody>\n"
                + table_rows
                + "            </tbody>\n"
                + "          </table>\n"
                + "        </div><!-- panel panel-primary -->";


        return content_CFW;
    }
	
	/*
	* Writes every content to the html file
	*/
    public void writeFile() {

        generateContent();
        generateStructure();
        try {
            File file = new File("index.html");

            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(structure);
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
