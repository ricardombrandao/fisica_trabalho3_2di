/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Scare
 */
public class ImportFile {
    
    public static String Import(){
        String file = "";
        if (Dictionary.getInstance().currentLanguage() == Dictionary.Language.PORTUGUESE) {
            file = "/Bibliography/PT.txt";
        } else if (Dictionary.getInstance().currentLanguage() == Dictionary.Language.ENGLISH) {
            file = "/Bibliography/ING.txt";
        }
        
        String output = "";
        BufferedReader br = null;
        try {
            String sCurrentLine;

            br = new BufferedReader(new FileReader(file));
            while ((sCurrentLine = br.readLine()) != null) {
                output += sCurrentLine;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return output;
    }
    
    
}
